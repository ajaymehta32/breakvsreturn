package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Return2 {

    int a =4;
    int b =5;
    int c =6;

    public void abc(){

        if(a >3) {
            System.out.println(a);
        }

        if(b >3) {
            return;  // after this statement it it not going below if statemnt it is coming out of method..by only printing value of a..
        }

        if(c >3) {
            System.out.println(c);
        }

    }

    public static void main(String args[]) {
        Return2 return2 = new Return2();
        return2.abc();

    }
}
