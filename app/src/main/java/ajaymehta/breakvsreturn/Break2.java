package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Break2 {

    public void printNumbers() {

        for(int i=1; i<=10; i++) {

            if(i==8) {
                break;  // it is just effecting this loop...after break ..it is executing the remaning method (doesnt matter if method contain more loop ..it will get inside other more loops..in this same method..
            }

            System.out.println("Inside Loop  "+i);
        }
        System.out.println("Outside Loop");

        // starting of 2nd loop statement

        for(int i=0; i<10; i++) {

            System.out.println("Inside Loop 2  "+(char) (i+97));  // printing ASCII+9
        }
        System.out.println("Outside Loop 2");

    } // end of method

    public static void main(String args[]) {

        Break2 break2 = new Break2();
        break2.printNumbers();
    }
}
