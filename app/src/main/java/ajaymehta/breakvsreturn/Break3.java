package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Break3 {

    public static void printNumbers(){

        for(int i=1; i<=5; i++){

            for(int j=1; j<=10; j++) {

                System.out.println(i+" X "+j+" = "+i*j);
            } // end of j looop

            System.out.println("====================");
            if(i==3){
                break;  // even after executing break statement in I ..it is still printing below System.out line..means after break statement cursor come outside that loop..n go through the whole remaining method...
            }

        }  // eno of i loop

        System.out.println("Break outside J loop..But inside I loop");

    }

    public static void main(String args[]) {

        printNumbers();


    }
}
