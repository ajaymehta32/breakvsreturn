package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Break1 {  // we are checking break statement when we have one loop insde a method..lets check in another program when we have two different loops inside single method

    public void printNumbers() {

        for(int i=1; i<=10; i++) {

            if(i==8) {
                break;
            }

            System.out.println("Inside Loop  "+i);
        }
        System.out.println("Outside Loop"); // after break statement ..it is coming outside loop in printNumbers method n printing System.out statemetnt

    }

    public static void main(String args[]) {

        Break1 break1 = new Break1();
        break1.printNumbers();
    }
}
