package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Return1 {
    public void printNumbers() {

        for(int i=1; i<=10; i++) {

            if(i==8) {
                return;  // return statement not only come our of loop..it come out of method...when a return statemtn executes..it doent print rest of the code..inside the method..it come out of the method...
            }

            System.out.println("Inside Loop  "+i);
        }
        System.out.println("Outside Loop");

    } // end of method

    public void printHello(){
        System.out.println("Hello...");
    }

    public static void main(String args[]) {

        Return1 return1 = new Return1();
        return1.printNumbers();
        return1.printHello();

    }
}
