package ajaymehta.breakvsreturn;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Break4 {  // see we have break in our j loop...so it is break j ..but our i loop is working smothly..
    // conclusion is ... break keyword..only effects its own loop ..not any other loop..regradless of nesting of loop....

    public static void printNumbers(){

        for(int i=1; i<=5; i++){

            for(int j=1; j<=10; j++) {

                System.out.println(i+" X "+j+" = "+i*j);

                if(j==5){ // IT WILL  make our every table go upto 5 only...
                    break; // this break will not effect I loop...it is only effect j loop
                }
            } // end of j looop

            System.out.println("====================");


        }  // eno of i loop

        System.out.println("Break In J loop..");

    }

    public static void main(String args[]) {

        printNumbers();


    }
}
